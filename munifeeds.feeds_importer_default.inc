<?php

/**
 * Implementation of hook_feeds_importer_default().
 */
function munifeeds_feeds_importer_default() {
  $export = array();
  $feeds_importer = new stdClass;
  $feeds_importer->disabled = FALSE; /* Edit this to true to make a default feeds_importer disabled initially */
  $feeds_importer->api_version = 1;
  $feeds_importer->id = 'commerce';
  $feeds_importer->config = array(
    'name' => 'commerce',
    'description' => '',
    'fetcher' => array(
      'plugin_key' => 'FeedsHTTPFetcher',
      'config' => array(
        'auto_detect_feeds' => FALSE,
        'use_pubsubhubbub' => FALSE,
        'designated_hub' => '',
      ),
    ),
    'parser' => array(
      'plugin_key' => 'FeedsXPathParserXML',
      'config' => array(
        'sources' => array(
          'xpathparser:0' => 'Name',
          'xpathparser:1' => 'Name',
          'xpathparser:2' => 'Address/Street',
          'xpathparser:3' => 'Address/City',
          'xpathparser:4' => 'Address/Prov',
          'xpathparser:5' => 'Address/Pcode',
          'xpathparser:6' => '',
          'xpathparser:7' => '@id',
          'xpathparser:8' => '@id',
          'xpathparser:9' => 'GeoCode/Latitude',
          'xpathparser:10' => 'GeoCode/Longitude',
          'xpathparser:11' => 'Distance',
        ),
        'rawXML' => array(
          'xpathparser:0' => 0,
          'xpathparser:1' => 0,
          'xpathparser:2' => 0,
          'xpathparser:3' => 0,
          'xpathparser:4' => 0,
          'xpathparser:5' => 0,
          'xpathparser:6' => 0,
          'xpathparser:7' => 0,
          'xpathparser:8' => 0,
          'xpathparser:9' => 0,
          'xpathparser:10' => 0,
          'xpathparser:11' => 0,
        ),
        'context' => '/SearchResults/Listings/Listing',
        'exp' => array(
          'errors' => 0,
          'tidy' => 0,
          'tidy_encoding' => 'UTF8',
          'debug' => array(
            'context' => 0,
            'xpathparser:0' => 0,
            'xpathparser:1' => 0,
            'xpathparser:2' => 0,
            'xpathparser:3' => 0,
            'xpathparser:4' => 0,
            'xpathparser:5' => 0,
            'xpathparser:6' => 0,
            'xpathparser:7' => 0,
            'xpathparser:8' => 0,
            'xpathparser:9' => 0,
            'xpathparser:10' => 0,
            'xpathparser:11' => 0,
          ),
        ),
      ),
    ),
    'processor' => array(
      'plugin_key' => 'FeedsNodeProcessor',
      'config' => array(
        'content_type' => 'commerce',
        'input_format' => '0',
        'update_existing' => '2',
        'expire' => '-1',
        'mappings' => array(
          0 => array(
            'source' => 'parent:og_groups',
            'target' => 'og_groups',
            'unique' => FALSE,
          ),
          1 => array(
            'source' => 'parent:language',
            'target' => 'language',
            'unique' => FALSE,
          ),
          2 => array(
            'source' => 'xpathparser:0',
            'target' => 'title',
            'unique' => FALSE,
          ),
          3 => array(
            'source' => 'xpathparser:1',
            'target' => 'field_adresse:name',
            'unique' => FALSE,
          ),
          4 => array(
            'source' => 'xpathparser:2',
            'target' => 'field_adresse:street',
            'unique' => FALSE,
          ),
          5 => array(
            'source' => 'xpathparser:3',
            'target' => 'field_adresse:city',
            'unique' => FALSE,
          ),
          6 => array(
            'source' => 'xpathparser:4',
            'target' => 'field_adresse:province',
            'unique' => FALSE,
          ),
          7 => array(
            'source' => 'xpathparser:5',
            'target' => 'field_adresse:postal_code',
            'unique' => FALSE,
          ),
          8 => array(
            'source' => 'xpathparser:6',
            'target' => 'field_adresse:country',
            'unique' => FALSE,
          ),
          9 => array(
            'source' => 'xpathparser:7',
            'target' => 'guid',
            'unique' => 1,
          ),
          10 => array(
            'source' => 'xpathparser:8',
            'target' => 'field_id',
            'unique' => FALSE,
          ),
          11 => array(
            'source' => 'xpathparser:9',
            'target' => 'field_adresse:locpick][user_latitude',
            'unique' => FALSE,
          ),
          12 => array(
            'source' => 'xpathparser:10',
            'target' => 'field_adresse:locpick][user_longitude',
            'unique' => FALSE,
          ),
          13 => array(
            'source' => 'parent:taxonomy:3',
            'target' => 'taxonomy:3',
            'unique' => FALSE,
          ),
          14 => array(
            'source' => 'xpathparser:11',
            'target' => 'field_distance',
            'unique' => FALSE,
          ),
        ),
        'author' => 0,
      ),
    ),
    'content_type' => 'commerce_feed',
    'update' => 0,
    'import_period' => '-1',
    'expire_period' => 3600,
    'import_on_create' => 0,
  );

  $export['commerce'] = $feeds_importer;
  $feeds_importer = new stdClass;
  $feeds_importer->disabled = FALSE; /* Edit this to true to make a default feeds_importer disabled initially */
  $feeds_importer->api_version = 1;
  $feeds_importer->id = 'mamrot_main';
  $feeds_importer->config = array(
    'name' => 'mamrot main',
    'description' => '',
    'fetcher' => array(
      'plugin_key' => 'FeedsHTTPFetcher',
      'config' => array(
        'auto_detect_feeds' => FALSE,
        'use_pubsubhubbub' => FALSE,
        'designated_hub' => '',
      ),
    ),
    'parser' => array(
      'plugin_key' => 'FeedsXPathParserHTML',
      'config' => array(
        'sources' => array(
          'xpathparser:0' => 'td[3]/a',
          'xpathparser:1' => 'td[3]/a/@href',
        ),
        'rawXML' => array(
          'xpathparser:0' => 0,
          'xpathparser:1' => 0,
        ),
        'context' => '//tbody/tr',
        'exp' => array(
          'errors' => 1,
          'tidy' => 1,
          'tidy_encoding' => 'UTF8',
          'debug' => array(
            'context' => 0,
            'xpathparser:0' => 0,
            'xpathparser:1' => 0,
          ),
        ),
      ),
    ),
    'processor' => array(
      'plugin_key' => 'FeedsFeedNodeProcessor',
      'config' => array(
        'content_type' => 'mamrot_muni_feed',
        'update_existing' => 0,
        'mappings' => array(
          0 => array(
            'source' => 'xpathparser:0',
            'target' => 'title',
            'unique' => FALSE,
          ),
          1 => array(
            'source' => 'xpathparser:1',
            'target' => 'source',
            'unique' => 1,
          ),
        ),
      ),
    ),
    'content_type' => 'mamrot_feed',
    'update' => 0,
    'import_period' => '-1',
    'expire_period' => 3600,
    'import_on_create' => 1,
  );

  $export['mamrot_main'] = $feeds_importer;
  $feeds_importer = new stdClass;
  $feeds_importer->disabled = FALSE; /* Edit this to true to make a default feeds_importer disabled initially */
  $feeds_importer->api_version = 1;
  $feeds_importer->id = 'mamrot_mrcs';
  $feeds_importer->config = array(
    'name' => 'mamrot mrcs',
    'description' => '',
    'fetcher' => array(
      'plugin_key' => 'FeedsHTTPFetcher',
      'config' => array(
        'auto_detect_feeds' => FALSE,
        'use_pubsubhubbub' => FALSE,
        'designated_hub' => '',
      ),
    ),
    'parser' => array(
      'plugin_key' => 'FeedsXPathParserHTML',
      'config' => array(
        'sources' => array(
          'xpathparser:0' => '.',
          'xpathparser:1' => './@value',
        ),
        'rawXML' => array(
          'xpathparser:0' => 'xpathparser:0',
          'xpathparser:1' => 'xpathparser:1',
        ),
        'context' => '//select[@id=\'mrc-cm-arg\']/option',
        'exp' => array(
          'errors' => 1,
          'tidy' => 1,
          'tidy_encoding' => 'UTF8',
          'debug' => array(
            'context' => 0,
            'xpathparser:0' => 0,
            'xpathparser:1' => 0,
          ),
        ),
      ),
    ),
    'processor' => array(
      'plugin_key' => 'FeedsFeedNodeProcessor',
      'config' => array(
        'content_type' => 'mrc_feed',
        'update_existing' => 0,
        'mappings' => array(
          0 => array(
            'source' => 'xpathparser:0',
            'target' => 'title',
            'unique' => FALSE,
          ),
          1 => array(
            'source' => 'xpathparser:1',
            'target' => 'source',
            'unique' => 1,
          ),
        ),
      ),
    ),
    'content_type' => 'mamrot_mrcs_feed',
    'update' => 0,
    'import_period' => '-1',
    'expire_period' => 3600,
    'import_on_create' => 0,
  );

  $export['mamrot_mrcs'] = $feeds_importer;
  $feeds_importer = new stdClass;
  $feeds_importer->disabled = FALSE; /* Edit this to true to make a default feeds_importer disabled initially */
  $feeds_importer->api_version = 1;
  $feeds_importer->id = 'mamrot_muni';
  $feeds_importer->config = array(
    'name' => 'mamrot_muni',
    'description' => '',
    'fetcher' => array(
      'plugin_key' => 'FeedsHTTPFetcher',
      'config' => array(
        'auto_detect_feeds' => FALSE,
        'use_pubsubhubbub' => FALSE,
        'designated_hub' => '',
      ),
    ),
    'parser' => array(
      'plugin_key' => 'FeedsXPathParserHTML',
      'config' => array(
        'sources' => array(
          'xpathparser:1' => '//h2[@class=\'margin-bottom\']',
          'xpathparser:3' => '//div[@id=\'onglet-information\']/ul[1]/li[3]',
          'xpathparser:4' => '//div[@id=\'onglet-information\']/ul[2]/li[3]',
          'xpathparser:5' => '//div[@id=\'onglet-information\']/ul[2]/li[4]/a',
          'xpathparser:6' => '//div[@id=\'onglet-information\']/ul[2]/li[5]',
          'xpathparser:12' => '//div[@id=\'onglet-information\']/ul[3]/li[6]',
          'xpathparser:13' => '//div[@id=\'onglet-information\']/ul[3]/li[7]',
          'xpathparser:14' => '//div[@id=\'onglet-information\']/ul[1]/li[1]',
          'xpathparser:15' => '//div[@id=\'onglet-information\']/ul[1]/li[1]',
          'xpathparser:16' => '//div[@id=\'onglet-information\']/ul[3]/li[1]',
          'xpathparser:17' => '//div[@id=\'onglet-information\']/ul[3]/li[2]',
          'xpathparser:18' => '//div[@id=\'onglet-information\']/ul[3]/li[4]',
          'xpathparser:19' => '//div[@id=\'onglet-information\']/ul[3]/li[5]',
          'xpathparser:20' => '',
          'xpathparser:21' => '//div[@id=\'c503\']/div[@class=\'repertoire\']/div[@class=\'dans-cette-page\']/p[2]',
          'xpathparser:22' => '//div[@id=\'c503\']/div[@class=\'repertoire\']/div[@class=\'dans-cette-page\']/p[2]',
          'xpathparser:23' => '//div[@id=\'c503\']/div[@class=\'repertoire\']/div[@class=\'dans-cette-page\']/p',
          'xpathparser:24' => '//div[@id=\'c503\']/div[@class=\'repertoire\']/div[@class=\'dans-cette-page\']/p',
          'xpathparser:25' => '//div[@id=\'onglet-information\']/ul[3]/li[3]',
          'xpathparser:26' => '//div[@id=\'onglet-information\']/ul[1]/li[2]',
          'xpathparser:27' => '//div[@id=\'onglet-information\']/ul[2]/li[1]/a',
          'xpathparser:28' => '//div[@id=\'onglet-organisation\']/ul[1]/li[1]',
          'xpathparser:29' => '//div[@id=\'onglet-organisation\']/ol[1]/li',
          'xpathparser:30' => '//div[@id=\'onglet-organisation\']/ul[2]/li',
          'xpathparser:31' => '//div[@id=\'onglet-information\']/ul[2]/li[2]/a',
          'xpathparser:32' => '//div[@id=\'c503\']/div[@class=\'repertoire\']/div[@class=\'dans-cette-page\']/p[@class=\'liens\']/a[1]/@href',
          'xpathparser:33' => '//div[@id=\'c503\']/div[@class=\'repertoire\']/div[@class=\'dans-cette-page\']/p[@class=\'liens\']/a[2]/@href',
          'xpathparser:34' => '',
          'xpathparser:35' => '//div[@id=\'c503\']/div[@class=\'repertoire\']/div[@class=\'dans-cette-page\']/p[2]',
          'xpathparser:36' => '',
        ),
        'rawXML' => array(
          'xpathparser:21' => 'xpathparser:21',
          'xpathparser:22' => 'xpathparser:22',
          'xpathparser:23' => 'xpathparser:23',
          'xpathparser:24' => 'xpathparser:24',
          'xpathparser:35' => 'xpathparser:35',
          'xpathparser:1' => 0,
          'xpathparser:3' => 0,
          'xpathparser:4' => 0,
          'xpathparser:5' => 0,
          'xpathparser:6' => 0,
          'xpathparser:12' => 0,
          'xpathparser:13' => 0,
          'xpathparser:14' => 0,
          'xpathparser:15' => 0,
          'xpathparser:16' => 0,
          'xpathparser:17' => 0,
          'xpathparser:18' => 0,
          'xpathparser:19' => 0,
          'xpathparser:20' => 0,
          'xpathparser:25' => 0,
          'xpathparser:26' => 0,
          'xpathparser:27' => 0,
          'xpathparser:28' => 0,
          'xpathparser:29' => 0,
          'xpathparser:30' => 0,
          'xpathparser:31' => 0,
          'xpathparser:32' => 0,
          'xpathparser:33' => 0,
          'xpathparser:34' => 0,
          'xpathparser:36' => 0,
        ),
        'context' => '//div[@id=\'wrap-droite\']',
        'exp' => array(
          'errors' => 0,
          'tidy' => 1,
          'tidy_encoding' => 'UTF8',
          'debug' => array(
            'context' => 0,
            'xpathparser:1' => 0,
            'xpathparser:3' => 0,
            'xpathparser:4' => 0,
            'xpathparser:5' => 0,
            'xpathparser:6' => 0,
            'xpathparser:12' => 0,
            'xpathparser:13' => 0,
            'xpathparser:14' => 0,
            'xpathparser:15' => 0,
            'xpathparser:16' => 0,
            'xpathparser:17' => 0,
            'xpathparser:18' => 0,
            'xpathparser:19' => 0,
            'xpathparser:20' => 0,
            'xpathparser:21' => 0,
            'xpathparser:22' => 0,
            'xpathparser:23' => 0,
            'xpathparser:24' => 0,
            'xpathparser:25' => 0,
            'xpathparser:26' => 0,
            'xpathparser:27' => 0,
            'xpathparser:28' => 0,
            'xpathparser:29' => 0,
            'xpathparser:30' => 0,
            'xpathparser:31' => 0,
            'xpathparser:32' => 0,
            'xpathparser:33' => 0,
            'xpathparser:34' => 0,
            'xpathparser:35' => 0,
            'xpathparser:36' => 0,
          ),
        ),
      ),
    ),
    'processor' => array(
      'plugin_key' => 'FeedsNodeProcessor',
      'config' => array(
        'content_type' => 'muni',
        'input_format' => '0',
        'update_existing' => '2',
        'expire' => '-1',
        'mappings' => array(
          0 => array(
            'source' => 'xpathparser:1',
            'target' => 'title',
            'unique' => FALSE,
          ),
          1 => array(
            'source' => 'xpathparser:3',
            'target' => 'field_gentile',
            'unique' => FALSE,
          ),
          2 => array(
            'source' => 'xpathparser:4',
            'target' => 'field_recensement_canada',
            'unique' => FALSE,
          ),
          3 => array(
            'source' => 'xpathparser:5',
            'target' => 'field_communaute_metropolitaire',
            'unique' => FALSE,
          ),
          4 => array(
            'source' => 'xpathparser:6',
            'target' => 'field_election_provinciale',
            'unique' => FALSE,
          ),
          5 => array(
            'source' => 'xpathparser:12',
            'target' => 'field_mode_election',
            'unique' => FALSE,
          ),
          6 => array(
            'source' => 'xpathparser:13',
            'target' => 'field_division_territoriale',
            'unique' => FALSE,
          ),
          7 => array(
            'source' => 'xpathparser:14',
            'target' => 'guid',
            'unique' => 1,
          ),
          8 => array(
            'source' => 'xpathparser:15',
            'target' => 'field_geocode',
            'unique' => FALSE,
          ),
          9 => array(
            'source' => 'xpathparser:16',
            'target' => 'field_area',
            'unique' => FALSE,
          ),
          10 => array(
            'source' => 'xpathparser:17',
            'target' => 'field_pop',
            'unique' => FALSE,
          ),
          11 => array(
            'source' => 'xpathparser:18',
            'target' => 'field_changement_regime:start',
            'unique' => FALSE,
          ),
          12 => array(
            'source' => 'xpathparser:19',
            'target' => 'field_prochaine_election:start',
            'unique' => FALSE,
          ),
          13 => array(
            'source' => 'xpathparser:20',
            'target' => 'field_adresse:name',
            'unique' => FALSE,
          ),
          14 => array(
            'source' => 'xpathparser:21',
            'target' => 'field_adresse:street',
            'unique' => FALSE,
          ),
          15 => array(
            'source' => 'xpathparser:22',
            'target' => 'field_adresse:city',
            'unique' => FALSE,
          ),
          16 => array(
            'source' => 'xpathparser:23',
            'target' => 'field_adresse:phone',
            'unique' => FALSE,
          ),
          17 => array(
            'source' => 'xpathparser:24',
            'target' => 'field_adresse:fax',
            'unique' => FALSE,
          ),
          18 => array(
            'source' => 'xpathparser:25',
            'target' => 'field_date_constitution:start',
            'unique' => FALSE,
          ),
          19 => array(
            'source' => 'xpathparser:26',
            'target' => 'field_designation',
            'unique' => FALSE,
          ),
          20 => array(
            'source' => 'xpathparser:27',
            'target' => 'field_region_administrative',
            'unique' => FALSE,
          ),
          21 => array(
            'source' => 'xpathparser:28',
            'target' => 'field_maire',
            'unique' => FALSE,
          ),
          22 => array(
            'source' => 'xpathparser:29',
            'target' => 'field_conseillers',
            'unique' => FALSE,
          ),
          23 => array(
            'source' => 'xpathparser:30',
            'target' => 'field_responsables',
            'unique' => FALSE,
          ),
          24 => array(
            'source' => 'xpathparser:31',
            'target' => 'field_mrc:title',
            'unique' => FALSE,
          ),
          25 => array(
            'source' => 'xpathparser:32',
            'target' => 'field_email',
            'unique' => FALSE,
          ),
          26 => array(
            'source' => 'xpathparser:33',
            'target' => 'field_siteweb:url',
            'unique' => FALSE,
          ),
          27 => array(
            'source' => 'xpathparser:34',
            'target' => 'field_adresse:province',
            'unique' => FALSE,
          ),
          28 => array(
            'source' => 'xpathparser:35',
            'target' => 'field_adresse:postal_code',
            'unique' => FALSE,
          ),
          29 => array(
            'source' => 'xpathparser:36',
            'target' => 'field_adresse:country',
            'unique' => FALSE,
          ),
        ),
        'author' => '1',
      ),
    ),
    'content_type' => 'mamrot_muni_feed',
    'update' => 0,
    'import_period' => '-1',
    'expire_period' => 3600,
    'import_on_create' => 1,
  );

  $export['mamrot_muni'] = $feeds_importer;
  $feeds_importer = new stdClass;
  $feeds_importer->disabled = FALSE; /* Edit this to true to make a default feeds_importer disabled initially */
  $feeds_importer->api_version = 1;
  $feeds_importer->id = 'mrc_feed';
  $feeds_importer->config = array(
    'name' => 'mrc feed',
    'description' => '',
    'fetcher' => array(
      'plugin_key' => 'FeedsHTTPFetcher',
      'config' => array(
        'auto_detect_feeds' => FALSE,
        'use_pubsubhubbub' => FALSE,
        'designated_hub' => '',
      ),
    ),
    'parser' => array(
      'plugin_key' => 'FeedsXPathParserHTML',
      'config' => array(
        'sources' => array(
          'xpathparser:0' => '//h2[@class="margin-bottom"]',
          'xpathparser:1' => '//div[@id="onglet-organisation"]/ul[1]/li[1]',
          'xpathparser:5' => '//div[@id="onglet-organisation"]/ul[2]/li',
          'xpathparser:6' => '//div[@id=\'c503\']/div[@class=\'repertoire\']/div[@class=\'dans-cette-page\']/p[2]',
          'xpathparser:7' => '//div[@id=\'c503\']/div[@class=\'repertoire\']/div[@class=\'dans-cette-page\']/p[2]',
          'xpathparser:8' => '//div[@id=\'c503\']/div[@class=\'repertoire\']/div[@class=\'dans-cette-page\']/p[2]',
          'xpathparser:9' => '//div[@id=\'c503\']/div[@class=\'repertoire\']/div[@class=\'dans-cette-page\']/p',
          'xpathparser:10' => '//div[@id=\'c503\']/div[@class=\'repertoire\']/div[@class=\'dans-cette-page\']/p',
          'xpathparser:11' => '//div[@id=\'c503\']/div[@class=\'repertoire\']/div[@class=\'dans-cette-page\']/p[@class=\'liens\']/a[2]/@href',
          'xpathparser:24' => '//div[@id="onglet-organisation"]/ul[1]/li[2]/a',
          'xpathparser:25' => '//div[@id=\'c503\']/div[@class=\'repertoire\']/div[@class=\'dans-cette-page\']/p[@class=\'liens\']/a[1]/@href',
          'xpathparser:26' => '//div[@id="onglet-organisation"]/ul[1]/li[1]',
        ),
        'rawXML' => array(
          'xpathparser:6' => 'xpathparser:6',
          'xpathparser:7' => 'xpathparser:7',
          'xpathparser:8' => 'xpathparser:8',
          'xpathparser:9' => 'xpathparser:9',
          'xpathparser:10' => 'xpathparser:10',
          'xpathparser:0' => 0,
          'xpathparser:1' => 0,
          'xpathparser:5' => 0,
          'xpathparser:11' => 0,
          'xpathparser:24' => 0,
          'xpathparser:25' => 0,
          'xpathparser:26' => 0,
        ),
        'context' => '//div[@id=\'c11\']/div',
        'exp' => array(
          'errors' => 0,
          'tidy' => 1,
          'tidy_encoding' => 'UTF8',
          'debug' => array(
            'context' => 0,
            'xpathparser:0' => 0,
            'xpathparser:1' => 0,
            'xpathparser:5' => 0,
            'xpathparser:6' => 0,
            'xpathparser:7' => 0,
            'xpathparser:8' => 0,
            'xpathparser:9' => 0,
            'xpathparser:10' => 0,
            'xpathparser:11' => 0,
            'xpathparser:24' => 0,
            'xpathparser:25' => 0,
            'xpathparser:26' => 0,
          ),
        ),
      ),
    ),
    'processor' => array(
      'plugin_key' => 'FeedsNodeProcessor',
      'config' => array(
        'content_type' => 'mrc',
        'input_format' => '0',
        'update_existing' => '2',
        'expire' => '-1',
        'mappings' => array(
          0 => array(
            'source' => 'xpathparser:0',
            'target' => 'title',
            'unique' => FALSE,
          ),
          1 => array(
            'source' => 'xpathparser:1',
            'target' => 'guid',
            'unique' => 1,
          ),
          2 => array(
            'source' => 'xpathparser:5',
            'target' => 'field_direction',
            'unique' => FALSE,
          ),
          3 => array(
            'source' => 'xpathparser:6',
            'target' => 'field_location:street',
            'unique' => FALSE,
          ),
          4 => array(
            'source' => 'xpathparser:7',
            'target' => 'field_location:city',
            'unique' => FALSE,
          ),
          5 => array(
            'source' => 'xpathparser:8',
            'target' => 'field_location:postal_code',
            'unique' => FALSE,
          ),
          6 => array(
            'source' => 'xpathparser:9',
            'target' => 'field_location:fax',
            'unique' => FALSE,
          ),
          7 => array(
            'source' => 'xpathparser:10',
            'target' => 'field_location:phone',
            'unique' => FALSE,
          ),
          8 => array(
            'source' => 'xpathparser:11',
            'target' => 'field_siteweb:url',
            'unique' => FALSE,
          ),
          9 => array(
            'source' => 'xpathparser:24',
            'target' => 'field_region_administrative',
            'unique' => FALSE,
          ),
          10 => array(
            'source' => 'xpathparser:25',
            'target' => 'field_email',
            'unique' => FALSE,
          ),
          11 => array(
            'source' => 'xpathparser:26',
            'target' => 'field_geocode_mrc',
            'unique' => FALSE,
          ),
          12 => array(
            'source' => 'xpathparser:27',
            'target' => 'field_location:province',
            'unique' => FALSE,
          ),
          13 => array(
            'source' => 'xpathparser:28',
            'target' => 'field_location:country',
            'unique' => FALSE,
          ),
        ),
        'author' => 0,
      ),
    ),
    'content_type' => 'mrc_feed',
    'update' => 0,
    'import_period' => '-1',
    'expire_period' => 3600,
    'import_on_create' => 0,
  );

  $export['mrc_feed'] = $feeds_importer;
  $feeds_importer = new stdClass;
  $feeds_importer->disabled = FALSE; /* Edit this to true to make a default feeds_importer disabled initially */
  $feeds_importer->api_version = 1;
  $feeds_importer->id = 'news_feed';
  $feeds_importer->config = array(
    'name' => 'news feed',
    'description' => '',
    'fetcher' => array(
      'plugin_key' => 'FeedsHTTPFetcher',
      'config' => array(
        'auto_detect_feeds' => 0,
        'use_pubsubhubbub' => 1,
        'designated_hub' => '',
      ),
    ),
    'parser' => array(
      'plugin_key' => 'FeedsSyndicationParser',
      'config' => array(),
    ),
    'processor' => array(
      'plugin_key' => 'FeedsNodeProcessor',
      'config' => array(
        'content_type' => 'news',
        'input_format' => '0',
        'update_existing' => '0',
        'expire' => '2419200',
        'mappings' => array(
          0 => array(
            'source' => 'parent:og_groups',
            'target' => 'og_groups',
            'unique' => FALSE,
          ),
          1 => array(
            'source' => 'parent:language',
            'target' => 'language',
            'unique' => FALSE,
          ),
          2 => array(
            'source' => 'guid',
            'target' => 'guid',
            'unique' => 1,
          ),
          3 => array(
            'source' => 'url',
            'target' => 'url',
            'unique' => 1,
          ),
          4 => array(
            'source' => 'title',
            'target' => 'title',
            'unique' => FALSE,
          ),
          5 => array(
            'source' => 'description',
            'target' => 'body',
            'unique' => FALSE,
          ),
          6 => array(
            'source' => 'timestamp',
            'target' => 'created',
            'unique' => FALSE,
          ),
          7 => array(
            'source' => 'url',
            'target' => 'field_source:url',
            'unique' => FALSE,
          ),
        ),
        'author' => '1',
      ),
    ),
    'content_type' => 'news_feed',
    'update' => 0,
    'import_period' => '-1',
    'expire_period' => 3600,
    'import_on_create' => 1,
  );

  $export['news_feed'] = $feeds_importer;
  $feeds_importer = new stdClass;
  $feeds_importer->disabled = FALSE; /* Edit this to true to make a default feeds_importer disabled initially */
  $feeds_importer->api_version = 1;
  $feeds_importer->id = 'topos_feed';
  $feeds_importer->config = array(
    'name' => 'topos feed',
    'description' => '',
    'fetcher' => array(
      'plugin_key' => 'FeedsHTTPFetcher',
      'config' => array(
        'auto_detect_feeds' => FALSE,
        'use_pubsubhubbub' => FALSE,
        'designated_hub' => '',
      ),
    ),
    'parser' => array(
      'plugin_key' => 'FeedsXPathParserHTML',
      'config' => array(
        'sources' => array(
          'xpathparser:0' => '//span[@id=\'ctl00_ConteneurToposWeb_vueFiche_etq9\']',
          'xpathparser:1' => '',
          'xpathparser:2' => '//span[@id=\'ctl00_ConteneurToposWeb_vueFiche_etq19\']',
          'xpathparser:3' => '//span[@id=\'ctl00_ConteneurToposWeb_vueFiche_etq21\']',
          'xpathparser:4' => '//span[@id=\'ctl00_ConteneurToposWeb_vueFiche_Label3\']',
          'xpathparser:5' => '//span[@id=\'ctl00_ConteneurToposWeb_vueFiche_etq5\']',
          'xpathparser:6' => '//span[@id=\'ctl00_ConteneurToposWeb_vueFiche_etq26\']',
          'xpathparser:7' => '//span[@id=\'ctl00_ConteneurToposWeb_vueFiche_etqSource\']',
          'xpathparser:8' => '//span[@id=\'ctl00_ConteneurToposWeb_vueFiche_etq17\']',
        ),
        'rawXML' => array(
          'xpathparser:0' => 0,
          'xpathparser:1' => 0,
          'xpathparser:2' => 0,
          'xpathparser:3' => 0,
          'xpathparser:4' => 0,
          'xpathparser:5' => 0,
          'xpathparser:6' => 0,
          'xpathparser:7' => 0,
          'xpathparser:8' => 0,
        ),
        'context' => '//body',
        'exp' => array(
          'errors' => 0,
          'tidy' => 1,
          'tidy_encoding' => 'UTF8',
          'debug' => array(
            'context' => 0,
            'xpathparser:0' => 0,
            'xpathparser:1' => 0,
            'xpathparser:2' => 0,
            'xpathparser:3' => 0,
            'xpathparser:4' => 0,
            'xpathparser:5' => 0,
            'xpathparser:6' => 0,
            'xpathparser:7' => 0,
            'xpathparser:8' => 0,
          ),
        ),
      ),
    ),
    'processor' => array(
      'plugin_key' => 'FeedsNodeMultisourceProcessor',
      'config' => array(
        'mappings' => array(
          0 => array(
            'source' => 'xpathparser:0',
            'target' => 'field_toponymie',
            'unique' => FALSE,
          ),
          1 => array(
            'source' => 'xpathparser:2',
            'target' => 'field_specifique_toponymie',
            'unique' => FALSE,
          ),
          2 => array(
            'source' => 'xpathparser:3',
            'target' => 'field_generique_toponymie',
            'unique' => FALSE,
          ),
          3 => array(
            'source' => 'xpathparser:5',
            'target' => 'field_carte_topo_50',
            'unique' => FALSE,
          ),
          4 => array(
            'source' => 'xpathparser:6',
            'target' => 'field_carte_topo_20',
            'unique' => FALSE,
          ),
          5 => array(
            'source' => 'xpathparser:7',
            'target' => 'field_source_toponymie',
            'unique' => FALSE,
          ),
          6 => array(
            'source' => 'xpathparser:8',
            'target' => 'field_date_topo:start',
            'unique' => FALSE,
          ),
        ),
        'content_type' => 'muni',
      ),
    ),
    'content_type' => 'topos_feed',
    'update' => 0,
    'import_period' => '-1',
    'expire_period' => 3600,
    'import_on_create' => 0,
  );

  $export['topos_feed'] = $feeds_importer;
  return $export;
}
